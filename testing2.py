import json
import urllib
import wget
import hashlib
import pyodbc
import datetime as dt
import os


class User_Client:
    sql_DELETE = '''
        DELETE FROM dbo.{table_name} WHERE 1 = 1
        '''
    sql_INSERT = '''
        INSERT INTO dbo.{table_name} ({entries}) VALUES ({values})
        '''

    def __init__(self):
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                   'Server=DESKTOP-1JJ8EJ1;'
                                   'Database=Evergreen;'
                                   'Trusted_Connection=yes;')

    def connect_to_db(self):
        return self.conn.cursor()

    def get_user_data(self):
        url = 'https://evergreen-life-interview.s3.eu-west-2.amazonaws.com/users.json'
        response = urllib.request.urlopen(url)
        return response.read().decode('utf-8')

    def get_user_data_json():
        wget.download(
            url='https://evergreen-life-interview.s3.eu-west-2.amazonaws.com/users.json')

    def create_dict_list_users(self, users):
        users_string = "["
        open_breakets_counter = 0
        close_breakets_counter = 0
        for line in users:
            users_string += line
            if "{" in line:
                open_breakets_counter += 1
            if "}" in line:
                close_breakets_counter += 1
            if open_breakets_counter == close_breakets_counter and close_breakets_counter > 0 and open_breakets_counter > 0:
                users_string += ","
                open_breakets_counter = 0
                close_breakets_counter = 0
        users_string += "]"
        users_dict = list(eval(users_string))
        return users_dict

    def hash_personal_data(self, users_list):
        for user in users_list:
            for key in user:
                if key == "givenName" or key == "familyName" or key == "dateOfBirth":
                    hash = hashlib.new("sha512_256")
                    hash.update(user[key].encode("utf-8"))
                    user[key] = hash.hexdigest()
        return users_list

    def insert_in_db(self, table_name, users_list):
        cursor = self.connect_to_db()

        for user in users_list:
            columns = []
            values = []
            for key in user:
                if key != 'address':
                    columns.append(key)
                    values.append(user[key])
                elif key == 'address':
                    for address_key in user[key]:
                        columns.append(address_key)
                        values.append(user[key][address_key])

            columns.append('last_updated')
            columns_for_sql = ', '.join(f'''[{entry}]''' for entry in columns)

            values.append(dt.datetime.now())
            values_tuple = tuple(values)
            dynamic_values = ', '.join('?' * len(columns))

            sql = self.sql_INSERT.format(
                table_name=table_name, entries=columns_for_sql, values=dynamic_values)
            cursor.execute(sql, values_tuple)
            cursor.commit()

    def empty_table(self, table_name):
        cursor = self.connect_to_db()
        sql = self.sql_DELETE.format(table_name)
        cursor.execute(sql)
        cursor.commit()

    def iterate_rows(self, users_list):
        for index in range(0, len(users_list), 500):
            yield [tuple(encrypted_user.values) for encrypted_user in users_list[index:index + 500]]


if __name__ == '__main__':
    user = User_Client()
    data = user.get_user_data()
    user_dict = user.create_dict_list_users(data)
    encrypted_users = user.hash_personal_data(user_dict)
    # print(encrypted_users)
    # table_name = 'Users'
    # user.insert_in_db(table_name, encrypted_users)
    print(user.iterate_rows(encrypted_users))
