from merger import Merger
from user_data import User_Client
from postcode_client import Postcode_Client


class Process:
    def __init__(self, user_client, postcode_client):
        self.user_client = user_client
        self.postcode_client = postcode_client


if __name__ == '__main__':
    # We define our objects
    user_client = User_Client()
    postcode_client = Postcode_Client()
    merger_client = Merger()

    # This gets the data from the website, transforms it into a list of dictionaries and encrypts the personal information
    data = user_client.get_user_data()
    user_dict = user_client.create_dict_list_users(data)
    encrypted_users = user_client.hash_personal_data(user_dict)
    # Test the update feature
    print(encrypted_users[1])
    encrypted_users[1]['address']['postcode'] = 'test_update'
    print(encrypted_users[1]['address']['postcode'])
    # This adds the user data into the DB
    table_name = 'Users'
    user_client.insert_in_db(table_name, encrypted_users)

    # We define our variables for updating/merging
    source_table_users = 'Users'
    source_table_postcodes = 'Postcodes'
    target_table = 'Merged_Table'
    join_elements_users = ['email', 'email', 'dateOfBirth', 'dateOfBirth']
    join_elements_merge = ['postcode', 'pcd']
    columns_no_PK_users = ['givenName', 'familyName',
                           'sex', 'dateOfBirth', 'street', 'postcode', 'last_updated']
    columns_to_insert_users = ['email', 'givenName', 'familyName',
                               'sex', 'dateOfBirth', 'street', 'postcode', 'last_updated']
    column_list = ['X', 'Y', 'objectid', 'pcd', 'pcd2', 'pcds', 'dointr', 'doterm', 'usertype', 'oseast1m', 'osnrth1m', 'osgrdind', 'oa11', 'cty', 'laua', 'ward', 'hlthau', 'ctry', 'pcon', 'eer',
                   'teclec', 'ttwa', 'pct', 'nuts', 'park', 'lsoa11', 'msoa11', 'wz11', 'ccg', 'bua11', 'buasd11', 'ru11ind', 'oac11', 'lat', 'long', 'lep1', 'lep2', 'pfa', 'imd', 'ced', 'nhser', 'rgn', 'calncv', 'stp']
    table_columns = columns_to_insert_users + column_list

    # Create table
    if not postcode_client.check_table_exists("Merged_Table"):
        columns = columns_to_insert_users + column_list
        postcode_client.create_table(columns, 'Merged_Table')

    # This adds new users to the table/updates existing users
    merger_client.update_table(
        source_table_users, target_table, join_elements_users, columns_no_PK_users, columns_to_insert_users)
    user_client.empty_table(source_table_users)

    # We match the postcode with the user location
    merger_client.merge_tables(
        source_table_postcodes, target_table, join_elements_merge, column_list)
