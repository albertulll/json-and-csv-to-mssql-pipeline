import pandas as pd
import pyodbc
import csv
import logging

logger = logging.getLogger(__name__)


class Postcode_Client:
    CREATE_sql = '''
    CREATE TABLE {table} ({columns})
    '''

    INSERT_sql = '''
    INSERT INTO [dbo].[Postcodes] ({columns}) VALUES ({values})
    '''

    TABLES_EXISTS_sql = '''
    SELECT COUNT(*)
    FROM information_schema.tables
    WHERE table_name = '{tablename}'
    '''

    def __init__(self):
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                   'Server=DESKTOP-1JJ8EJ1;'
                                   'Database=Evergreen;'
                                   'Trusted_Connection=yes;')

    def connect_to_db(self):
        return self.conn.cursor()

    def create_table(self, columns_list, table_name):
        cursor = self.connect_to_db()

        columns_for_sql = " , ".join(
            f'{column} nvarchar(250)'
            if column != 'last_updated' else f'{column} datetime'
            for column in columns_list)

        sql = self.CREATE_sql.format(table=table_name, columns=columns_for_sql)
        cursor.execute(sql)
        cursor.commit()
        logger.info('Table Created')

    def insert_data(self, columns_list, file_path):
        cursor = self.connect_to_db()

        columns_for_sql = ', '.join(
            f'''[{column}]''' for column in columns_list)
        dynamic_values = ', '.join('?' * len(columns_list))
        SQL = self.INSERT_sql.format(
            columns=columns_for_sql, values=dynamic_values)

        with open(file_path, 'r') as f:
            reader = csv.reader(f)
            next(reader)
            for data in reader:
                data_tuple = tuple(data)
                cursor.execute(SQL, data_tuple)
            cursor.commit()
            logger.info('Data inserted.')

    def check_table_exists(self, table_name):
        cursor = self.connect_to_db()
        sql = self.TABLES_EXISTS_sql.format(tablename=table_name)
        cursor.execute(sql)
        if cursor.fetchone()[0] == 1:
            cursor.close()
            return True

        cursor.close()
        return False
