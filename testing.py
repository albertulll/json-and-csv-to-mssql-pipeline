from urllib import request
import urllib
from urllib.request import urlopen

# import json
import json
# store the URL in url as
# parameter for urlopen
url = 'https://evergreen-life-interview.s3.eu-west-2.amazonaws.com/users.json'

# store the response of URL
response = urllib.request.urlopen(url)

# storing the JSON response
# from url in data
data_json = response.read

# print the json response
print(data_json)
