import pyodbc
import logging

logger = logging.getLogger(__name__)


class Merger:
    UPDATE_SQL = '''
        MERGE dbo.{target_table} as target
        USING (SELECT DISTINCT * FROM dbo.{source_table}) as source
        ON {join_condition}
        WHEN MATCHED THEN UPDATE SET {fields_joining_condition}
        WHEN NOT MATCHED THEN INSERT ({columns}) VALUES ({values});
    '''
    MERGE_SQL = '''
        MERGE dbo.{target_table} as target
        USING (SELECT DISTINCT * FROM dbo.{source_table}) as source
        ON {join_condition}
        WHEN MATCHED THEN UPDATE SET {fields_joining_condition};
    '''

    def __init__(self):
        self.conn = pyodbc.connect('Driver={SQL Server};'
                                   'Server=DESKTOP-1JJ8EJ1;'
                                   'Database=Evergreen;'
                                   'Trusted_Connection=yes;')

    def connect_to_db(self):
        return self.conn.cursor()

    def update_table(self, source_table, target_table, join_elements, columns_no_PK, columns_to_insert):
        cursor = self.connect_to_db()

        if len(join_elements) == 2:
            join_condition = ''.join(
                f'target.{join_elements[0]} = source.{join_elements[1]}')
        elif len(join_elements) == 4:
            join_condition = ''.join(
                f'target.{join_elements[0]} = source.{join_elements[1]} AND target.{join_elements[2]} = source.{join_elements[3]}')

        fields_joining_condition = ', '.join(
            f'target.{column} = source.{column}' for column in columns_no_PK)
        columns_sql = ', '.join(f'{column}' for column in columns_to_insert)
        values_sql = ', '.join(
            f'source.{column}' for column in columns_to_insert)

        sql = self.UPDATE_SQL.format(
            target_table=target_table,
            source_table=source_table,
            join_condition=join_condition,
            fields_joining_condition=fields_joining_condition,
            columns=columns_sql,
            values=values_sql
        )
        logger.info('Inserting/updating users')
        cursor.execute(sql)
        cursor.commit()

    def merge_tables(self, source_table, target_table, join_elements, columns_no_PK):
        cursor = self.connect_to_db()
        join_condition = ''.join(
            f'target.{join_elements[0]} = source.{join_elements[1]}')
        fields_joining_condition = ', '.join(
            f'target.{column} = source.{column}' for column in columns_no_PK)

        sql = self.MERGE_SQL.format(
            target_table=target_table,
            source_table=source_table,
            join_condition=join_condition,
            fields_joining_condition=fields_joining_condition
        )
        logger.info('Joining poscodes table into the destination table')
        cursor.execute(sql)
        cursor.commit()
